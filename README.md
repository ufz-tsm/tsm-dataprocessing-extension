# tsm-dataprocessing-extension


# CI?CD Pipeline

In the last deployment stage that only runs on `main`, a job is added that contains a
trigger to inform the https://codebase.helmholtz.cloud/ufz-tsm/tsm-extractor
about the successful run of the pipline.
This job *can* be run manually (by clicking the play button) to make the
extractor build a new image with the requirements from this repository.

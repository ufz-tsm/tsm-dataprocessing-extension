#!/usr/bin/env python3


import tsm_user_code.example  # noqa, keep it


def register_all():
    """Register all user written functions"""
    # This is a dummy function. During import each function with a
    # @flagging, @processing or @register decorator is automatically
    # available on each SaQC instance.
    # This function just ensures that all above imports were executed.
    return

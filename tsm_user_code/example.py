#!/usr/bin/env python3
"""
Module that hold user defined SaQC functions.

Functions must use the processing, flagging or register
decorator to be registered in the SaQC class.

Registered functions can be called as if they are native
SaQC methods. Example:
```
@saqc.core.flagging()
def doNothing(qc:saqc.SaQC, field: str, **kwargs):
    return qc

data = saqc.SaQC(...)
data.doNothing(field='foo')
```
"""
from __future__ import annotations
import numpy as np
import pandas as pd
from saqc.core import processing
import saqc


@processing()
def ext_setFirst(
    qc: saqc.SaQC,
    field: str,
    n: int,
    value: float = np.nan,
    **kwargs,  # pylint: disable=unused-argument
):
    """Set the first n observations to a constant value"""
    data: pd.Series = qc._data[field]
    data.iloc[:n] = value
    qc._data[field] = data
    return qc

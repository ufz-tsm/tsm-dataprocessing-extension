#!/usr/bin/env python3

import os
import sys

d = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.insert(0, os.path.abspath(d))
del d

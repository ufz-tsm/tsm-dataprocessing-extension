#!/usr/bin/env python
from __future__ import annotations

import numpy as np
import pandas as pd
import pytest
import saqc
from tsm_user_code.example import ext_setFirst  # noqa


def mk_qc(data, name):
    """Generate a SaQC object with 1 column and datetime index."""
    idx = pd.date_range("2000", None, periods=len(data), freq="1d")
    s = pd.Series(data, index=idx, name=name)
    return saqc.SaQC(s)


@pytest.mark.parametrize(
    "data, kargs, expected",
    [
        ([5, 5, 5], {"n": 0, "value": 99}, [5, 5, 5]),
        ([5, 5, 5], {"n": 100, "value": 99}, [99, 99, 99]),
        ([], {"n": 100, "value": 99}, []),
        ([5, 5, 5], {"n": 1, "value": np.nan}, [np.nan, 5, 5]),
        ([5, 5, 5], {"n": 1}, [np.nan, 5, 5]),
        ([5, 5, 5], {"n": 1, "value": None}, [np.nan, 5, 5]),
    ],
)
def test_setFirst(data, kargs, expected):
    """Test function setFirst for general and empty case."""
    field = f"data-{len(data)}"
    qc = mk_qc(data, field)
    expected = mk_qc(expected, field)
    result = ext_setFirst(qc, field, **kargs)
    r: pd.Series = result.data[field]
    e: pd.Series = expected.data[field]
    assert r.equals(e)
